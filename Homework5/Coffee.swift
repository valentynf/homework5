//
//  Coffee.swift
//  Homework5
//
//  Created by Valentyn Filippov on 8/30/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import UIKit

class Coffee: NSObject {
    
    
    var water = 100 //ml
    var beans = 25 // g
    var milk = 10 //ml
    var coffeeName : String
    override var description: String {
        let result = "This coffee contains \(water)ml of water, \(beans)g of beans and \(milk)ml of milk"
        return result
    }
    
    init(drinkName : String) {
        coffeeName = drinkName
    }
    
}
