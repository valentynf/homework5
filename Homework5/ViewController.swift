//
//  ViewController.swift
//  Homework5
//
//  Created by Valentyn Filippov on 8/30/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var waterStatus: UILabel!
    @IBOutlet weak var beansStatus: UILabel!
    @IBOutlet weak var milkStatus: UILabel!
    
    let coffeeMachine = CoffeeMachine.init()
    let americano = Coffee.init(drinkName: "americano")
    let capuchino = Coffee.init(drinkName: "capuchino")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Sorry in advance for a horrible coffee - not a big fan of this drink - I prefer alcohol
        
        capuchino.water = 300
        capuchino.beans = 150
        capuchino.milk = 50
        
        coffeeMachine.milkCont = UserDefaults.standard.integer(forKey: "milkInMachine")
        coffeeMachine.beansCont = UserDefaults.standard.integer(forKey: "beansInMachine")
        coffeeMachine.waterTank = UserDefaults.standard.integer(forKey: "waterInMachine")
        
        updateTheStatus(coffeeMachine: coffeeMachine)
        
        centerLabel.text = "Welcome" // just to remove boring LABEL. service is everything
        
    }
    
    @IBAction func capucinoButtonIsClicked(_ sender: UIButton) {
        centerLabel.text = coffeeMachine.makeCoffee(coffee: capuchino)
        updateTheStatus(coffeeMachine: coffeeMachine)
    }
    
    @IBAction func americanoButtonIsCliked(_ sender: UIButton) {
        centerLabel.text = coffeeMachine.makeCoffee(coffee: americano)
        updateTheStatus(coffeeMachine: coffeeMachine)
    }
    
    @IBAction func fillWaterIsClicked(_ sender: UIButton) {
        centerLabel.text = coffeeMachine.fillWaterTank()
        updateTheStatus(coffeeMachine: coffeeMachine)
    }
    
    @IBAction func fillBeansIsClicked(_ sender: UIButton) {
        centerLabel.text = coffeeMachine.fillBeans()
        updateTheStatus(coffeeMachine: coffeeMachine)
    }
    
    @IBAction func fillMilkIsClicked(_ sender: UIButton) {
        centerLabel.text = coffeeMachine.fillMilk()
        updateTheStatus(coffeeMachine: coffeeMachine)
    }
    
    func updateTheStatus (coffeeMachine : CoffeeMachine) {
        waterStatus.text = "\(coffeeMachine.waterTank) ml"
        beansStatus.text = "\(coffeeMachine.beansCont) g"
        milkStatus.text = "\(coffeeMachine.milkCont) ml"
        
        UserDefaults.standard.set(coffeeMachine.waterTank, forKey: "waterInMachine")
        UserDefaults.standard.set(coffeeMachine.beansCont, forKey: "beansInMachine")
        UserDefaults.standard.set(coffeeMachine.milkCont, forKey: "milkInMachine")
        
    } // refreshes the status of coffeemachine ingridients and write them to the FILE!
    
}

