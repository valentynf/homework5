//
//  CoffeeMachine.swift
//  Homework5
//
//  Created by Valentyn Filippov on 8/30/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    var waterTank = 0 // ml
    var beansCont = 0 // g
    var milkCont = 0 // ml
    
    func notEnoughWater (water : Int) -> Bool {
        if waterTank < water {
            return true
        } else {
            return false
        }
    }
    
    func notEnoughMilk (milk: Int) -> Bool {
        if milkCont < milk {
            return true
        } else {
            return false
        }
    }
    
    func notEnoughBeans (beans : Int) -> Bool {
        if beansCont < beans {
            return true
        } else {
            return false
        }
    }
    
    func fillWaterTank() -> String {
        waterTank = waterTank + 500
        return "You have \(waterTank)ml of water"
    }
    
    func fillBeans () -> String {
        beansCont = beansCont + 150
        return "You have \(beansCont)g of beans"
    }
    
    func fillMilk () -> String {
        milkCont = milkCont + 100
        return "You have \(milkCont)ml of milk"
    }
    
    override init() {
        // nothing
    }
    
    func makeCoffee (coffee : Coffee) -> String {
        if (notEnoughWater(water: coffee.water) == false) {
            if (notEnoughBeans(beans: coffee.beans) == false) {
                if (notEnoughMilk(milk: coffee.milk) == false) {
                    milkCont = milkCont - coffee.milk
                    beansCont = beansCont - coffee.beans
                    waterTank = waterTank - coffee.water
                    return "Your \(coffee.coffeeName) is ready."
                } else {
                    return "Not enough milk!"
                }
            } else {
                return "Not enough beans!"
            }
        } else {
            return "Not enough water!"
        }
    }
    
}
